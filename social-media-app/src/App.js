
import './App.css';

import Register from './pages/register/Register';
import Login from './pages/login/Login';
import{ createBrowserRouter, RouterProvider,Route, Outlet, Navigate} from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import LeftBar from './components/leftBar/LeftBar';
import RightBar from './components/rightBar/RightBar';
import Home from './pages/home/Home';
import Profile  from './pages/profile/Profile';
import "./style.scss"
import {useContext} from "react";
import { DarkModeContext } from './context/darkModeContext';

function App() {
   const currentUser = true;
   const ProtectedRoute = ({children}) =>{
    if(!currentUser){
      return <Navigate to='/login'/>
    }
    return children;
   }
   const {darkMode} = useContext(DarkModeContext);

  const Layout = () =>{
    return(
      <div className ={`theme-${darkMode ? "dark" : "light"}`}>
        <Navbar/>
        <div style={{display: 'flex'}}>
          <LeftBar/>
          <div style={{flex:6}}>
          <Outlet/>
          </div>
         
          <RightBar/>
        </div>
      </div>
    )
  }
  const router = createBrowserRouter([
    {path: '/', element : <ProtectedRoute><Layout/></ProtectedRoute>,
  children : [
    {path: '/',
  element: <Home/>},
    {path: '/profile/:id',
  element: <Profile/>},
  
  ]},
    
    {path: '/register', element: <Register/>,},
    {path: '/login', element: <Login/>,}
  
  ])
  return (
    <div className="App">
      <RouterProvider router={router}></RouterProvider>
    </div>
  );
}

export default App;
